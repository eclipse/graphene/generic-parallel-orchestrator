# ===================================================================================
# Copyright (C) 2021 Peter Schueller. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by Peter Schueller
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END==========================================================

import concurrent.futures
import grpc
import json
import logging
import os
import queue
import re
import signal
import time
import traceback
from typing import Generator

import orchestrator_pb2
import orchestrator_pb2_grpc
from ai4eu.orchestrator import Core as OrchestratorCore
import ai4eu.othread as othread
from ai4eu.othread import OrchestrationObserver, Event


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def graceful_shutdown(signum, frame):
    logger.info("Received termination signal. Shutting down gracefully...")
    grpc_server.stop(0)

signal.signal(signal.SIGTERM, graceful_shutdown)

class QueueOrchestrationObserver(OrchestrationObserver):
    def __init__(self, event_queue: queue.Queue):
        self.event_queue = event_queue

    def event(self, evt: Event):
        self.event_queue.put(evt)

class OrchestratorServicerImpl(orchestrator_pb2_grpc.OrchestratorServicer):
    def __init__(self):
        super().__init__()

        self.event_queue = queue.Queue()
        self.observer = QueueOrchestrationObserver(self.event_queue)

        self.oc = None
        self.om = None
        self.threads = {}
        self.queues = {}
        self.status = orchestrator_pb2.OrchestrationStatus(message="not initialized")

    def _kill_threads_remove_queues(self):
        if self.om is not None:
            self.observer.event(Event(name='terminate_orchestration', component='orchestrator', detail={}))
            logging.error("terminate orch")
            self.om.terminate_orchestration()

        if len(self.threads) > 0:
            self.observer.event(Event(name='delete_threads', component='orchestrator', detail={}))
            for tid, t in list(self.threads.items()):
                t.input_queue = None
                t.output_queue = None
                del(self.threads[tid])
        assert len(self.threads) == 0

        if len(self.queues) > 0:
            self.observer.event(Event(name='delete_queues', component='orchestrator', detail={}))
            for qid, q in list(self.queues.items()):
                del(self.queues[qid])
        assert len(self.queues) == 0

        self.oc = None
        self.om = None

    def get_status_string(self):
        return f'message: {self.status.message} - active_threads: {self.status.active_threads} - success: {self.status.success} - code: {self.status.code}'

    def initialize(self, request: orchestrator_pb2.OrchestrationConfiguration, context) -> orchestrator_pb2.OrchestrationStatus:
        try:
            logging.info("initialize %s", request)
            # do a clean start
            self.status = orchestrator_pb2.OrchestrationStatus(message="initializing...")
            self._kill_threads_remove_queues()

            # interpret input jsons
            bpjson = json.loads(request.blueprint)
            dijson = json.loads(request.dockerinfo)

            # create new core
            self.observer.event(Event(name='create_core', component='orchestrator', detail={}))
            self.oc = OrchestratorCore()
            nodes = self.oc.merge_protobuf_collect_node_infos(bpjson, dijson, request.protofiles)
            rpcs, links = self.oc.collect_rpc_and_link_infos(bpjson)

            # create manager
            self.observer.event(Event(name='create_manager', component='orchestrator', detail={}))
            # can be imported only here because is created by merge_protobuf_collect_node_infos above
            import all_in_one_pb2
            import all_in_one_pb2_grpc
            self.om = othread.OrchestrationManager(all_in_one_pb2, all_in_one_pb2_grpc, observer=self.observer)

            # create one thread for each rpc with an input queue
            self.observer.event(Event(name='create_threads', component='orchestrator', detail={}))
            self.threads = {}
            for rpc in rpcs:
                # identifier
                rpcid = rpc.identifier()
                logging.debug("creating thread for rpc %s", rpcid)
                assert rpcid not in self.threads, 'rpcid must be unique in the solution'

                # create
                t = self.om.create_thread(
                    component=rpc.node.container_name,
                    stream_in=rpc.input.stream,
                    stream_out=rpc.output.stream,
                    empty_in=rpc.input.name == 'Empty',
                    empty_out=rpc.output.name == 'Empty',
                    host=rpc.node.host, port=rpc.node.port,
                    service=rpc.node.service_name,
                    rpc=rpc.operation,
                )


                if rpc.input.name != 'Empty':
                    # create input queue and attach
                    q = self.om.create_queue(
                        name=rpcid,
                        message=rpc.input.name,
                    )
                    t.attach_input_queue(q)

                # register thread
                self.threads[rpcid] = t

            # for each link, add input queue of receiver to list of output queues of sender
            self.observer.event(Event(name='register_queues', component='orchestrator', detail={}))
            for link in links:
                # input/output is seen differently from link and thread perspective!
                # the input of the link is the output of a thread
                # the output of the link is the input of a thread
                # here we use the link perspective

                # threads
                input_from_rpcid = link.input.identifier()
                output_to_rpcid = link.output.identifier()

                input_thread = self.threads[input_from_rpcid]
                output_thread = self.threads[output_to_rpcid]

                # each thread at the output of a link has one input queue
                queue = output_thread.input_queue

                # we connect the output of a thread at the input of the queue to the queue
                input_thread.attach_output_queue(queue)

            self.observer.event(Event(name='initialized', component='orchestrator', detail={}))

            self.status.success = False
            self.status.code = 0
            self.status.message = "initialized"
            self.active_threads = len(self.threads)


        except Exception as e:
            logging.info("OSI initialize exception: %s", traceback.format_exc())
            self.observer.event(Event(name='exception', component='orchestrator', detail={'method': 'initialize', 'traceback': traceback.format_exc()}))
            self.status.success = False
            self.status.code = -1
            self.status.message = "OSI initialize exception: "+traceback.format_exc()
            self.active_threads = len(self.threads)

        logging.info("OSI initialize returning %s", self.get_status_string())
        return self.status

    def observe(self, request: orchestrator_pb2.OrchestrationObservationConfiguration, context) -> Generator[orchestrator_pb2.OrchestrationEvent, None, None]:
        try:
            logging.info("OSI observe %s", request)

            namefilter = re.compile(request.name_regex)
            componentfilter = re.compile(request.component_regex)

            # exit if the connection dies
            while context.is_active():

                # try to get orchestrator event from queue
                try:
                    oevt = self.event_queue.get(block=True, timeout=1.0)

                    # create yield event
                    logging.debug("OSI event %s", oevt)
                    if namefilter.match(oevt.name) and componentfilter.match(oevt.component):
                        yevt = orchestrator_pb2.OrchestrationEvent(
                            run=oevt.run,
                            name=oevt.name,
                            component=oevt.component,
                            detail=oevt.detail,
                        )
                        logging.debug("OSI observe yielding %s", yevt)
                        yield yevt

                except queue.Empty:
                    pass

            logging.info("OSI observe exiting (context inactive)")
        except Exception as e:
            logging.info("OSI observe exception: %s", traceback.format_exc())

    def run(self, request: orchestrator_pb2.RunLabel, context) -> orchestrator_pb2.OrchestrationStatus:
        try:
            logging.info("OSI run %s", request)

            self.om.start_orchestration()

            self.status.success = False
            self.status.code = 0
            self.status.message = "running"
            self.active_threads = len(self.threads)

        except Exception as e:
            logging.info("OSI run exception: %s", traceback.format_exc())
            self.status.success = False
            self.status.code = -1
            self.status.message = "OSI run exception: "+traceback.format_exc()
            self.active_threads = len(self.threads)

        logging.info("OSI run returning %s", self.get_status_string())
        return self.status

    def stop_orchestration(self, request: orchestrator_pb2.RunLabel, context) -> orchestrator_pb2.OrchestrationStatus:
        try:
            logging.info("OSI stop orchestration %s", request)
            
            if self.om:
                self.om.stop_orchestration()

            self.status.success = True
            self.status.code = 0
            self.status.message = "orchestration stopped"
            self.active_threads = 0  # or calculate the actual number of active threads if needed

        except Exception as e:
            logging.error("OSI stop orchestration exception: %s", traceback.format_exc())
            self.status.success = False
            self.status.code = -1
            self.status.message = "OSI stop orchestration exception: " + traceback.format_exc()

        logging.info("OSI stop orchestration returning %s", self.get_status_string())
        return self.status

    def get_status(self, request: orchestrator_pb2.RunLabel, context) -> orchestrator_pb2.OrchestrationStatus:
        self.status.active_threads = 0
        started = 0 # a streaming pipeline could be running with all threads started and no message yet
        for t in self.threads.values():
            logging.info(f"checking status of thread {t.component} {t.rpcname} {t.last_event_name}")
            if t.is_running():
                self.status.active_threads += 1
            if t.last_event_name == "thread.started":
                started += 1
        if self.status.active_threads < 1 and started < len(self.threads):
            self.status.success=True
            self.status.code=0
        logging.info("OSI get_status returning %s", self.get_status_string())
        return self.status

def load_config():
    config_file = os.environ.get('CONFIG', "config.json")
    try:
        logger.info("Loading config from %s", config_file)
        return json.load(open(config_file, 'rt'))
    except Exception as e:
        logger.warning("Using empty config (=defaults) because %s", e)
        return {}


def start_server():
    server_config = load_config()
    grpc_server = grpc.server(
        concurrent.futures.ThreadPoolExecutor(max_workers=10),
        options=(
            ('grpc.keepalive_time_ms', 1000), # send each second
            ('grpc.keepalive_timeout_ms', 3000), # 3 second = timeout
            ('grpc.keepalive_permit_without_calls', True), # allow ping without RPC calls
            ('grpc.http2.max_pings_without_data', 0), # allow unlimited pings without data
            ('grpc.http2.min_time_between_pings_ms', 1000), # allow pings every second
            ('grpc.http2.min_ping_interval_without_data_ms', 1000), # allow pings without data every second
        )
    )
    orchestrator_pb2_grpc.add_OrchestratorServicer_to_server(OrchestratorServicerImpl(), grpc_server)
    grpc_port = server_config.get('grpcport', 8061)
    grpc_server.add_insecure_port(f'0.0.0.0:{grpc_port}')
    logger.info("Starting Orchestrator gRPC server at port %d", grpc_port)
    grpc_server.start()

    grpc_server.wait_for_termination()

if __name__ == "__main__":
    start_server()
